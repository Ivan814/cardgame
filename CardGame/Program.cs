﻿using System;
using System.Collections.Generic;
using System.Linq;
using CardGame.Model;
namespace CardGame
{
    class HandComparier : IComparer<Hand>
    {
        public int Compare(Hand a, Hand b)
        {
            if (a.Weight.Item1 ==  b.Weight.Item1) {
                return a.Weight.Item2 >  b.Weight.Item2 ? 1 : a.Weight.Item2  ==  b.Weight.Item2 ? 0: -1;
            }
            
            return a.Weight.Item1 > b.Weight.Item1 ? 1 :  a.Weight.Item1 ==  b.Weight.Item1? 0: -1;
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Deck myDeck = new Deck();
                myDeck.Shuffle();
                int start = 0;
                List<Hand> hands = new List<Hand>();
                int maxOrder = 0;
                int maxRemainder = 0;
                for(int i=0;i<5;i++)
                {
                    var hand = new Hand(myDeck.GetHands(start));
                    hands.Add(hand);
                    start += 5;
                    Console.WriteLine("Player " + i + ": ");
                    Console.WriteLine("\tHand Type: " + hand.GetHandType());
                    Console.WriteLine("\tCards: " + hand.ToString());
                }

                var winner = hands.OrderBy(i => i, new HandComparier()).Last();
                if (winner != null)
                {
                    int index = hands.FindIndex(h => h == winner);
                    Console.WriteLine("The winner is player " + index);
                }
                else
                {
                    Console.WriteLine("There is no winner");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}