using System;

namespace CardGame.Model
{
    public enum Suit
    {
        Club,
        Diamond,
        Heart,
        Spade
    }
    public enum CardRank
    {
        Two = 2,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight,
        Nine,
        Ten,
        Jack,
        Queen,
        King,
        Ace
    }
    
    public class Card
    {
        private Suit suit;
        private CardRank cardRank;
        public Card(Suit newSuit, CardRank newCardRank)
        {
            suit = newSuit;
            cardRank = newCardRank;
        }
        
        public override string ToString()
        {
            return (cardRank < CardRank.Ten? Convert.ToInt16(cardRank).ToString() : cardRank.ToString().Substring(0, 1))  + suit.ToString().Substring(0, 1);
            
        }
    }
}