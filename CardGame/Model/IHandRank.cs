using System;

namespace CardGame.Model
{public interface IHandRank
    {
        Tuple<int, double, string> Score(int order);
    } 
}