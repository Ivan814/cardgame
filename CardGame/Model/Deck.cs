using System;
using System.Collections.Generic;
using System.Linq;

namespace CardGame.Model
{
    public class Deck
    {
        private List<Card> cards;
        public Deck()
        {
            cards = new List<Card>(new Card[52]);
            for(int suitVal=0;suitVal<4;suitVal++)
            {
                for (int levelVal = 1; levelVal < 14; levelVal++)
                {
                    cards[suitVal * 13 + levelVal - 1] = new Card((Suit) suitVal, levelVal == 1 ? CardRank.Ace: (CardRank) levelVal);
                }
            }            
        }
        
        public Card this[int index]
        {
            get
            {
                if (index < 0 || index >= cards.Count)
                    throw new IndexOutOfRangeException("Index out of range");

                return cards[index];
            }
        }
        public void Shuffle()
        {
            Card[] newDeck = new Card[52];
            bool[] assigned = new bool[52];
            Random sourceGen = new Random();
            for(int i=0;i<52;i++)
            {
                int destCard = 0;
                bool foundCard = false;
                while(foundCard==false)
                {
                    destCard = sourceGen.Next(52);
                    if(assigned[destCard]==false)
                        foundCard = true;
                }
                assigned[destCard] = true;
                newDeck[destCard] = cards[i];
            }
            cards = newDeck.ToList();            
        }

        public List<Card> GetHands(int startIndex)
        {
            return cards.Skip(startIndex).Take(5).ToList();
        }
    }
}