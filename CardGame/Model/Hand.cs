using System;
using System.Collections.Generic;
using System.Linq;

namespace CardGame.Model
{
    public class Hand
    {
        
        readonly IEnumerable<IHandRank> _ranks = null;
        readonly IEnumerable<string> _cards = null;
        
        public Hand(IEnumerable<Card> cards) {
            if (cards?.Count() != 5 ) {
                throw new Exception();
            }
            _cards = cards.Select(h => h.ToString());
            
            _ranks = new List<IHandRank> {
                new High_Card(_cards),
                new One_Pair(_cards),
                new Two_Pairs(_cards),
                new Three_Of_Kinds(_cards),
                new Straight(_cards),
                new Flush(_cards),
                new Full_House(_cards),
                new Four_Of_Kinds(_cards),
                new Straight_Flush(_cards),
                new Royal_Flush(_cards)
            };
        }
        
        public Tuple<int,double, string> Weight {
            get {
                return  _ranks.Select((r, order) => r.Score(order))
                    .Where(r => r.Item2 > 0)
                    .OrderBy(r => r.Item1)
                    .Last();
            }
        }

        public override string ToString()
        {
            return String.Join(' ', _cards.OrderBy(c => HandRank.Mapper[c[0]]).ToArray());
        }

        public string GetHandType()
        {
            return String.Join(' ',this.Weight.Item3.Split(' '));
        }
    }
}